package com.gunawan.spritesheet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends AppCompatActivity {
    GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize gameView and set it as the view
        gameView = new GameView(this);
        setContentView(gameView);

    }

    class GameView extends SurfaceView implements Runnable {

        Thread gameThread = null;

        SurfaceHolder ourHolder;

        volatile boolean playing;

        Canvas canvas;
        Paint paint;

        long fps;

        private long timeThisFrame;

        Bitmap bitmapBob;

        boolean isMoving = false;

        float walkSpeedPerSecond = 500;

        float bobXPosition = 10;

        private int frameWidth = 300;
        private int frameHeight = 200;
        private int frameCount = 5;
        private int currentFrame = 0;

        private long lastFrameChangeTime = 0;
        private int frameLengthInMilliseconds = 100;

        Display display = getWindowManager().getDefaultDisplay();
        Point point = new Point();

        boolean isFlipped = false;

        private Rect frameToDraw = new Rect(
                0,
                0,
                frameWidth,
                frameHeight);

        RectF whereToDraw = new RectF(
                bobXPosition, 0,
                bobXPosition + frameWidth,
                frameHeight);

        public GameView(Context context) {
            super(context);

            ourHolder = getHolder();
            paint = new Paint();

            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);

            bitmapBob = Bitmap.createScaledBitmap(bitmapBob,
                    frameWidth * frameCount,
                    frameHeight,
                    false);
        }

        @Override
        public void run() {
            while (playing) {
                long startFrameTime = System.currentTimeMillis();

                update();
                draw();

                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1) {
                    fps = 1000 / timeThisFrame;
                }
            }

        }

        public void update() {
            display.getSize(point);

            if(isMoving){
                bobXPosition = bobXPosition + (walkSpeedPerSecond / fps);

                // Reverse movement
                if(bobXPosition >= point.x - (bitmapBob.getWidth() / frameCount)
                        || bobXPosition < 0) {

                    walkSpeedPerSecond = -walkSpeedPerSecond;
                    isFlipped = !isFlipped;
                }
            }
        }

        public void getCurrentFrame(){
            long time  = System.currentTimeMillis();
            if(isMoving) {
                if ( time > lastFrameChangeTime + frameLengthInMilliseconds) {
                    lastFrameChangeTime = time;
                    currentFrame++;
                    if (currentFrame >= frameCount) {
                        currentFrame = 0;
                    }
                }
            }
            frameToDraw.left = currentFrame * frameWidth;
            frameToDraw.right = frameToDraw.left + frameWidth;

        }

        public void draw() {
            if (ourHolder.getSurface().isValid()) {
                canvas = ourHolder.lockCanvas();
                canvas.drawColor(Color.argb(255,  26, 128, 182));

                paint.setColor(Color.argb(255,  249, 129, 0));
                paint.setTextSize(45);

                canvas.drawText("FPS:" + fps, 20, 40, paint);

                whereToDraw.set((int)bobXPosition,
                        frameHeight,
                        (int)bobXPosition + frameWidth,
                        frameHeight + bitmapBob.getHeight());

                getCurrentFrame();

                Matrix matrix = new Matrix();
                matrix.preScale(isFlipped ? -1 : 1, 1);

                Bitmap rotatedBob = Bitmap.createBitmap(bitmapBob, 0, 0 , bitmapBob.getWidth(),
                        bitmapBob.getHeight(), matrix, true);

                canvas.drawBitmap(rotatedBob,
                        frameToDraw,
                        whereToDraw, paint);

                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }
        }

        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    isMoving = true;
                    break;
                case MotionEvent.ACTION_UP:
                    isMoving = false;
                    break;
            }
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }
}
